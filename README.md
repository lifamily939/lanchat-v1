# LANChat V1


Using IRC server with WeeChat Client (not WeChat) over LAN

Client is opened through a terminal, preferably a bash shell.
In Ubuntu/Linux, this is easy.
In Windows, I recommend installing the "Ubuntu" app through the Windows Store, and following the instructions there, in order to use a terminal in Windows.



First, in your terminal environment, install WeeChat:

`sudo apt-get update`

`sudo apt-get install weechat`


Once WeeChat is installed, you should be able to run it:

`weechat`


Once WeeChat is opened, you will notice that this is a completely terminal-based application, with use primarily on the keyboard.

You need to add the IRC server, then connect to it.

In our case, the IRC server is located at `10.0.0.61/6697`
I recommend naming the server `lanchat`, although this can be anything you want. Also, the `-autoconnect` flag at the end of the command is so that the server you create is automatically connected to every time you start weechat from here on.

`/server add lanchat 10.0.0.61/6697 -autoconnect`

Next, connect to the server:

`/connect lanchat`

Once you are connected, weechat should generate a bunch of text like this that tells you that you are in:

```
|17:42:11 lanchat  -- | 10.0.0.61: Welcome to 939 LiFamily!  
│17:42:11 lanchat  -- | Welcome to the 939 LiFamily IRC Network 
```

At this point, you should set a nickname for yourself so that other users can recognize you.

`/nick <your name here>`

Once your nickname is set, you are ready to join a channel:

`/join #<channel_name>`

Note that the `#` is necesary to add in front of a channel name.


Here are the channels that I have already created for the LiFamily 939 LAN Chat:

```
#939
##939MainFloor
##939Basement
##939TopFloor
#939Lab1
#939Lab2
#939Lab3
#939Workshop1
#939Workshop2
#939Studio1
#939Studio2
#939BasementKitchen
#939LivingRoom
#939Office
#939DiningRoom
#939Kitchen
#939CalvinRoom
#939NaiNaiRoom
#939WaiPuoRoom
#939LindenRoom
#939MasterBedroom
```

To cycle through the channels that you are in, press
`Alt` and use the arrow keys.

To leave the channel you are in, enter
`/close`

To exit WeeChat, type
`/quit`








